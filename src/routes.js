import Dashboard from "views/Dashboard.js";

import WorldMap from "views/WorldMap.js";

import BarChartRace from "views/BarChartRace.js";
import Maps from "views/Map.js";


var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-bank",
    component: Dashboard,
    layout: "/admin",
  },
  {
    path: "/WorldMap",
    name: "World Map",
    icon: "nc-icon nc-diamond",
    component: WorldMap,
    layout: "/admin",
  },
  {
    path: "/maps",
    name: "Thailand Map",
    icon: "nc-icon nc-pin-3",
    component: Maps,
    layout: "/admin",
  },
  {
    path: "/BarChartRace",
    name: "BarChart Race",
    icon: "nc-icon nc-tile-56",
    component: BarChartRace,
    layout: "/admin",
  },
 
 
];
export default routes;
